### Description

This role is a basic role to deploy / start / manage a tshock based terraria server.  Most configuration options (config.json, and serverconfig.txt) are defined with reasonable defaults and can be overriden if desired.  At a minimum the server password should probably be overriden.  

Loosely based, initially, on this [guide](https://tshock.co/xf/index.php?threads/guide-how-to-host-tshock-on-linux.4798/)

Default downloads tshock from [here](https://github.com/Pryaxis/TShock/releases/latest) by default.

Includes start_server.sh / stop_server.sh and a handler to allow restarting server on configuration change (meant to be called from root).  Actual user running server is configurable; terraria user by default.  World is autocreated if not provided / pre-existing.

### OS's Tested/Supported

Debian

### Example Configuration:

~~~yaml
- hosts: 
    terraria
  become: yes
  roles:
  - role: terraria_tshock
    terraria_ServerPassword: "my_password"
    terraria_world_name: "My World"
    terraria_world_file: "my.wld"
~~~

### Possible Enhancements:

* Allow "upload world" via ansible role; or via link.
* Allow "server side characters" configuration via ansible role.
* Integrate Nginx to create a basic splash page [e.g. server(s), IPs] / world download link.  
* Allow deploying multiple servers?  (possibly another role.)    
* Modify start_server/stop_server to not assume running from root/superuser (maybe)
* Mirror tshock setting definitions from [here](https://tshock.readme.io/v4.3.22/docs/command-line-parameters)
* Generic admin interface?  (unlikely; but possible as a seperate project.)
* Improve daemonization/logging; screen works but is clunky.
* Syslog shipping destination option?
* Move away from using become; not entirely sure its necessary.